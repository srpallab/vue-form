#+title: Vue From Basic
#+author: Shafiqur Rahman

* Auto Importing Components
  - Why auto import?
    - Base for others, building Blocks
    - Manual Import gets repetitive

* Base Input
  In vue 3 we can have multiple root node.
  - Add label prop
  - Allow v-modeling: for allowing v-modeling we need to add
    ~modelValue~ prop into view, ~@input~ event binding with
    ~update:modelValue~ event and need emit it for v-model binding.
  - Bind ~$attrs~
  - Example
    #+BEGIN_SRC js
      <template>
	<label>{{label}}</label>
	<input v-bind="$attrs"
	       class="field"
	       :value="modelValue"
	       :placeholder="label"
	       @input="$emit('update:modelValue', $event.target.value)"
	/>
      </template>

      <script>
      export default {
	 name: "BaseInput",
	 props: {
	   label: {
	     type: String,
	     default: ''
	   },
	   modelValue: {
	     type: [String, Number],
	     default: ''
	   }
     
	 }
      };
      </script>

    #+END_SRC

* Base Select
  - Add label prop
  - Make it v-model capable
    - ~onChange~ event add
    - ~$emit(update:modelValue, $event.target.value)~
  - Add ~$attrs~
  - Make the options reactive
  - Add Selected logic
* Base Checkbox
  - Add label prop
  - Make it v-model capable
    - ~@change="$emit('update:modelValue', $event.target.checked)"~
* Base Radio Button
  - Add label props
  - Make it v-model capable
  - Add ~$attrs~
* Base Radio Group
  - Add options prop
  - Make options v-for loop
  - Add name props
  - Make v-model capable
* ~<component :is>~
  For wrapping the code in div or span depending on props value
  - Example:
    #+BEGIN_SRC html
      <component :is="vertical ? 'div' : 'span'">
	<MyComponent>
      </component>
    #+END_SRC
  - ~vertical === :vetrical="true"~
* Submitting
  Three main basics.
  - Add a From Tag
  - Add type submit to button
  - Prevent Default submit
    #+BEGIN_SRC html
      <form @submit.prevent="sendForm">

	<button type="submit"></button>
      </form>
    #+END_SRC
