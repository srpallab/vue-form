import { createApp } from "vue";
import App from "./App.vue";
// Adding lodash to project
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

// Adding all components start with Base
const requiredComponents = require.context(
  './components',
  false,
  /Base[A-Z]\w+\.(vue|js)$/
)

// Creating Vue App
const app = createApp(App)

requiredComponents.keys().forEach(fileName => {
  const componentConfig = requiredComponents(fileName)
  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, '$1'))
  )
  app.component(componentName, componentConfig.default || componentConfig)
}) 

// Mountig to index.html
app.mount("#app");
